import { combineReducers } from "redux";
import dataListReducer from './dataList';

const reducers = combineReducers({
    data: dataListReducer
})

export default reducers;