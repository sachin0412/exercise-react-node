import * as actionTypes from "../actions/actionTypes";

const INITIAL_STATE = {
    dataList: [],
    loading: false,
    error: null, 
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case actionTypes.DATA_START:
            return { 
                ...state, 
                loading: true,
                error: null,
            };
        case actionTypes.DATA_SUCCESS:
            return { 
                ...state, 
                dataList: action.payloadData,
                loading: false,
                error: null, 
            };
        case actionTypes.DATA_FAIL:
            return { 
                ...state, 
                loading: false,
                error: action.error,
            };
        
        default: return state;
    }
};
