import * as actionTypes from './actionTypes';
import axios from 'axios';

export const dataStart = () => {
    return {
        type: actionTypes.DATA_START
    }
};

export const dataSuccess = (data) => {
    return {
        type: actionTypes.DATA_SUCCESS,
        payloadData: data
    }
};

export const dataFail = (error) => {
    return {
        type: actionTypes.DATA_FAIL,
        error: error
    }
};

export const data = () => {
    return dispatch => {
        dispatch(dataStart());
        const url = "http://localhost:7000/datas";
        const TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';
        axios.get(url, { headers: { 
                'Content-Type': 'application/json',
                'auth-token': TOKEN,
            }})
            .then(response => {
                console.log("response:", response)
                dispatch(dataSuccess(response.data))
            })
            .catch(error => {
                console.log("error:", error)
                dispatch(dataFail(error))
            })
    }
}

