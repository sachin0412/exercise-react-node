import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { 
    Container,  Grid, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper,  CircularProgress
} from '@material-ui/core';

import * as actions from '../store/actions/index'

class ListData extends Component {
    componentDidMount() {
        this.props.onData()
    }

    render() {
        const {  dataList } = this.props;
        return (
            <Container className="mt-5">
                <Grid container spacing={0} >
                    <Grid item xs={12} sm={12} md={12} lg={12} className="">
                        <Paper>
                            {
                                dataList.length > 0 ?
                                    <TableContainer>
                                        <Table stickyHeader aria-label="sticky table">
                                            <TableHead className="">
                                                <TableRow>
                                                    <TableCell>ID</TableCell>
                                                    <TableCell>NAME</TableCell>
                                                    <TableCell>STATUS</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {
                                                    dataList.map( list => {
                                                        return (
                                                            <TableRow hover key={list.id}>
                                                                <TableCell className="py-0">
                                                                    <Link to={`/detail/${list.id}`}>
                                                                        {list.id}
                                                                    </Link>
                                                                </TableCell>
                                                                <TableCell className="py-0">{list.name}</TableCell>
                                                                <TableCell className="py-0">{list.status}</TableCell>
                                                            </TableRow>
                                                        )
                                                    })
                                                }
                                            </TableBody>
                                        </Table>
                                    </TableContainer>            
                                : <CircularProgress />
                            }
                        </Paper>
                    </Grid>
                </Grid>
            </Container>
        )
    }
};

const mapStateToProps = (state) => {
  return {           
    dataList: state.data.dataList,
  }         
};

const mapDispatchToProps = (dispatch) => {
  return {
    onData: () => dispatch(actions.data()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListData);
