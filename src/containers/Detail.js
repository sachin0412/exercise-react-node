import React, { Component } from 'react';
import { Container, Card, CardContent, Typography } from '@material-ui/core';
import axios from 'axios';

class Detail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            value: '',
            config: '',
            status: '',
            Desc: '',
            responseStatus: ''
        }
    }

    componentDidMount() {
        this.detailData()
    }

    detailData = () => {
        const url = `http://localhost:7000/datas/${this.props.match.params.id}`;
        axios.get(url)
            .then(response => {
                this.setState({
                    id: response.data.id,
                    name: response.data.name,
                    value: response.data.value,
                    config: response.data.config,
                    status: response.data.status,
                    Desc: response.data.Desc,
                    responseStatus: true
                })
            })
            .catch(error => {
                if (error) {
                    this.setState({
                        status: 'The data with the given ID was not found.',
                        responseStatus: false
                    })
                }
            })
    }
    render() {
        const { id, name, value, config, status, Desc, responseStatus } = this.state;
        return (
            <Container>
                <Card className="" style={{marginTop: 30 }}>
                {
                    responseStatus ?
                        <CardContent>
                            <Typography className="" color="textSecondary" gutterBottom>
                                <b>ID : </b> {id}
                            </Typography>
                            <Typography variant="h5" component="h2">
                                NAME : {name}
                            </Typography>
                            <Typography className="" color="textSecondary">
                                <b>VALUE : </b> {value}
                            </Typography>
                            <Typography variant="body2" component="p">
                                <b>Config : </b> {config}
                            </Typography>
                            <Typography variant="body2" component="p">
                                <b>Description : </b> {Desc}
                            </Typography>
                            <Typography variant="body2" component="p">
                                <b>STATUS : </b> {status}
                            </Typography>
                        </CardContent>
                    : 
                        <CardContent>
                            <Typography variant="body2" component="p">
                                <b>STATUS : </b> {status}
                            </Typography>
                        </CardContent>
                }
                </Card>
            </Container>
        )
    }
}

export default Detail;
