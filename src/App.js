import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import './App.css';
import Header from './components/Header';
import ListData from './containers/ListData';
import Detail from './containers/Detail';

class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <Header />
          <Switch>
            <Route path="/" exact component={ListData} />
            <Route path="/detail/:id" component={Detail} />
          </Switch>
        </BrowserRouter>
      </div>
    )
  }
};

export default App;
